import math
import cv2
import numpy as np

from client import *
from modules.networking import DisconnectedException
from server import *


def getCroppedImage(image, points_source):
    points_destination = np.float32(
        [[0, 0], [image.shape[1], 0], [image.shape[1], image.shape[0]], [0, image.shape[0]]])

    # Calculer la matrice de perspective
    matrix = cv2.getPerspectiveTransform(points_source, points_destination)

    # Appliquer la transformation de perspective
    image_perspective = cv2.warpPerspective(image, matrix, (image.shape[1], image.shape[0]))

    # Trouver le rectangle englobant les points de destination
    x, y, w, h = cv2.boundingRect(points_destination)

    # Recadrer l'image finale pour enlever les bords noirs
    return image_perspective[y:y + h, x:x + w], matrix

def merge(img1, img2):
    img1 = cv2.resize(src=img1, dsize=(img2.shape[1], img2.shape[0]))

    return np.vstack((img1, img2))



def findCircle(rawImg, matrix, myColor):
    image = cv2.cvtColor(rawImg, cv2.COLOR_BGR2GRAY)
    circles = cv2.HoughCircles(image=image,
                               method=cv2.HOUGH_GRADIENT,
                               dp=1,
                               minDist=5,
                               param1=110,
                               param2=39,
                               maxRadius=70)

    pos = list()

    if circles is not None:
        for co, i in enumerate(circles[0, :], start=1):
            x = int(i[0])
            y = int(i[1])
            r = int(i[2])

            if x != 0 or y != 0:
                myRobot = False
                if myColor[0]*0.9 < rawImg[y, x, 0] < myColor[0]*1.1 and myColor[1]*0.9 < rawImg[y, x, 1] < myColor[1]*1.1 and myColor[2]*0.9 < rawImg[y, x, 2] < myColor[2]*1.1:
                    myRobot = True

                pts = np.array([[x, y]], dtype="float32")
                pts = np.array([pts])

                pts = cv2.perspectiveTransform(pts, matrix)

                pos.append([pts[0][0][0], pts[0][0][1], r, myRobot])

    return pos


def drawCircle(img, circles, offset):

    for c in circles:
        c[1] += offset

        if c[3]:
            color = (0, 255, 0)
        else:
            color = (0, 0, 255)

        cv2.circle(img, (int(c[0]), int(c[1])), c[2], color, 2)

    return img


if __name__ == "__main__":
    points_source2110 = np.float32([[69, 21], [538, 16], [546, 453], [85, 468]])
    points_source2120 = np.float32([[103, 27], [580, 27], [568, 475], [108, 464]])
    myColor = (227, 194, 136)#(128, 176, 134)


    # camera2120 = cv2.imread("calibrate/terrain-2120.png")
    # camera2110 = cv2.imread("calibrate/terrain-2110.png")
    #
    #
    # cropped2120 = getCroppedImage(camera2120, points_source2120)
    # cropped2110 = getCroppedImage(camera2110, points_source2110)
    # merged      = merge(cropped2110, cropped2120, 0.8)
    #
    #
    # Afficher ou enregistrer l'image recadrée
    # cv2.imshow("terrain", merged)
    # cv2.waitKey()
    # cv2.destroyAllWindows()
    #
    # exit()



    client211O = Client()
    client212O = Client()

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--server', action='store', default='127.0.0.1', type=str,
                        help='address of server to connect')
    parser.add_argument('-p', '--port', action='store', default=12345, type=int, help='port on server')

    args2110 = parser.parse_args()
    args2110.server = "192.168.1.169"
    args2110.port   = 2110


    args2120        = parser.parse_args()
    args2120.server = "192.168.1.169"
    args2120.port   = 2120


    # serveur de communication avec le robot
    server = Server(2048)
    server.initialize("192.168.1.162", 32007, connexionCallBack)
    server.start()

    lastX = 0
    lastY = 0


    try:
        client211O.start(args2110)
        client212O.start(args2120)
        while client211O.connected and client212O.connected:
            if client211O.frame is not None and client212O.frame is not None:

                cropped2120, matrix2120 = getCroppedImage(client212O.frame, points_source2120)
                cropped2110, matrix2110 = getCroppedImage(client211O.frame, points_source2110)
                merged = merge(cropped2110, cropped2120)


                circlePos2110 = findCircle(client211O.frame, matrix2110, myColor)
                circlePos2120 = findCircle(client212O.frame, matrix2120, myColor)
                drawCircle(merged, circlePos2110, 0)
                drawCircle(merged, circlePos2120, cropped2110.shape[0])






                cv2.imshow("terrain", merged)
                key = cv2.waitKey(1) & 0x0FF

                if key == ord('x'):
                    break


        client211O.stop()
        client212O.stop()
    except DisconnectedException:
        print("Plantage d'un serveur et/ou de la connexion")
        client211O.stop()
        client212O.stop()






# def lineIntersection(l11, l12, l21, l22):
#     x = ((l11[0] * l12[1] - l12[0] * l11[1]) * (l21[0] - l22[0]) - (l11[0] - l12[0]) * (
#             l21[0] * l22[1] - l22[0] * l21[1]))
#     x /= (l11[0] - l12[0]) * (l21[1] - l22[1]) - (l11[1] - l12[1]) * (l21[0] - l22[0])
#
#     y = ((l11[0] * l12[1] - l12[0] * l11[1]) * (l21[1] - l22[1]) - (l11[1] - l12[1]) * (
#             l21[0] * l22[1] - l22[0] * l21[1]))
#     y /= (l11[0] - l12[0]) * (l21[1] - l22[1]) - (l11[1] - l12[1]) * (l21[0] - l22[0])
#
#     return int(x), int(y)


# def findBorderLines(lines):
#     bottom1 = (0, 0)
#     bottom2 = (0, 0)
#
#     top1 = (0, 1000)
#     top2 = (0, 1000)
#
#     left1 = (1000, 0)
#     left2 = (1000, 0)
#
#     right1 = (0, 0)
#     right2 = (0, 0)
#
#
#     if len(lines)==0:
#         return None, None, None, None, None, None, None, None
#
#     for i in range(len(lines)):
#         pt1 = lines[i][0]
#         pt2 = lines[i][1]
#         if abs(pt1[1] - pt2[1]) < 50:
#             if (pt1[1] > bottom1[1] and pt2[1] > bottom2[1]) or (pt2[1] > bottom1[1] and pt1[1] > bottom2[1]):
#                 bottom1 = lines[i][0]
#                 bottom2 = lines[i][1]
#
#             if pt1[1] < top1[1] and pt2[1] < top2[1]:
#                 top1 = lines[i][0]
#                 top2 = lines[i][1]
#
#         if abs(pt1[0] - pt2[0]) < 50:
#             if pt1[0] < left1[0] and pt2[0] < left2[0]:
#                 left1 = lines[i][0]
#                 left2 = lines[i][1]
#
#             if pt1[0] > right1[0] and pt2[0] > right2[0]:
#                 right1 = lines[i][0]
#                 right2 = lines[i][1]
#
#     return top1, top2, bottom1, bottom2, left1, left2, right1, right2




# class VideoCapture:
#
#     def __init__(self):
#         self.current_img = None
#         self.crop_img = None
#
#         self.topLeft = None
#         self.topRight = None
#         self.bottomLeft = None
#         self.bottomRight = None
#
#         self.color = []
#
#     def __del__(self):
#         r = 0
#         g = 0
#         b = 0
#
#         for c in self.color:
#             r += c[0]
#             g += c[1]
#             b += c[2]
#
#         r /= len(self.color)
#         g /= len(self.color)
#         b /= len(self.color)
#
#         print("r = ", r, " || g = ", g, " || b = ", b)
#
#     def findRobot(self):
#
#         self.cropImage()
#
#
#         image = cv2.cvtColor(self.crop_img, cv2.COLOR_BGR2RGB)
#
#         img_cpy = image.copy()
#
#         # convert image to grayscale
#         image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#         # apply a blur using the median filter
#         image = cv2.medianBlur(image, 5)
#
#         circles = cv2.HoughCircles(image=image, method=cv2.HOUGH_GRADIENT, dp=0.9, minDist=5, param1=110, param2=39,
#                                    maxRadius=70)
#
#         if circles is not None:
#             for co, i in enumerate(circles[0, :], start=1):
#                 # draw the outer circle in green
#                 x = int(i[0])
#                 y = int(i[1])
#                 r = int(i[2])
#
#                 if x !=0 or y !=0:
#                     color = (int(img_cpy[y, x, 0]), int(img_cpy[y, x, 1]), int(img_cpy[y, x, 2]))
#
#                     self.color.append(color)
#                     #print(color)
#
#                     # draw the center of the circle in red
#
#                     if 228*0.9 < color[0] < 228*1.1 and 56*0.9 < color[1] < 56*1.1 and 125*0.9 < color[2] < 125*1.1:
#                         cv2.circle(self.crop_img,(i[0],i[1]),2,(0,0,255),3)
#
#     def findTerrain(self):
#
#         if self.current_img is None:
#             print("Image is None")
#             return
#
#         dst = cv2.Canny(self.current_img, 50, 200, None, 3)
#
#         lines = cv2.HoughLines(dst, 1, np.pi / 180, 100, None, 0, 0)
#         tab_line = []
#
#         if lines is not None:
#             for i in range(0, len(lines)):
#                 rho = lines[i][0][0]
#                 theta = lines[i][0][1]
#                 a = math.cos(theta)
#                 b = math.sin(theta)
#                 x0 = a * rho
#                 y0 = b * rho
#                 pt1 = (int(x0 + 1000 * (-b)), int(y0 + 1000 * a))
#                 pt2 = (int(x0 - 1000 * (-b)), int(y0 - 1000 * a))
#
#                 tab_line.append([pt1, pt2])
#
#         top1, top2, bottom1, bottom2, left1, left2, right1, right2 = findBorderLines(tab_line)
#
#         if top1 is not None:
#             self.topLeft = lineIntersection(top1, top2, left1, left2)
#             self.topRight = lineIntersection(top1, top2, right1, right2)
#             self.bottomLeft = lineIntersection(bottom1, bottom2, left1, left2)
#             self.bottomRight = lineIntersection(bottom1, bottom2, right1, right2)
#
#
#     def cropImage(self):
#         # Coordinates that you want to Perspective Transform
#         input_pts = np.float32([[self.topLeft[0], self.topLeft[1]],
#                                 [self.topRight[0], self.topRight[1]],
#                                 [self.bottomLeft[0], self.bottomLeft[1]],
#                                 [self.bottomRight[0], self.bottomRight[1]]])
#
#         # Size of the Transformed Image
#         output_pts = np.float32([[0, 0],
#                                  [self.bottomLeft[1] - self.topLeft[1], 0],
#                                  [0, self.topRight[0] - self.topLeft[0]],
#                                  [self.bottomRight[1] - self.topRight[1], self.bottomRight[0] - self.bottomLeft[0]]])
#
#         max_h = max(int(self.bottomLeft[1] - self.topLeft[1]), int(self.bottomRight[1] - self.topRight[1]))
#         max_w = max(int(self.topRight[0] - self.topLeft[0]), int(self.bottomLeft[0] - self.bottomRight[0]))
#
#
#         M = cv2.getPerspectiveTransform(input_pts, output_pts)
#         self.crop_img = cv2.warpPerspective(self.current_img, M, (max_h, max_w))
#         # (self.bottomLeft[1] - self.topLeft[1], self.bottomRight[0] - self.bottomLeft[0])



