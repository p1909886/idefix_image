from modules.networking import TCPServerAbstraction
from modules.networking import Buffer


class Server(TCPServerAbstraction):

    def __init__(self, bufferSize):
        super().__init__(bufferSize) # a determiner

    def start(self):
        self.listenToClients()
        self.passiveReceive(receiveCallBack)
        print("Server started")




def receiveCallBack(s, b, server):
    if b is not None:
        print("Message reçu :", b.displayString())
        x = input("saisir x : ")
        y = input("saisir y : ")

        server.sendTo(s, Buffer("{}, {}".format(x, y).encode("utf-8")))




def connexionCallBack(socketClient, addresseClient="null"):
    # do nothing
    return 0

if __name__ == "__main__":

    server = Server(2048)
    server.initialize("192.168.1.162", 32007, connexionCallBack)
    server.start()


