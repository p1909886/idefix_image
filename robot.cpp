#include <iostream>
#include <string>
#include <array>
#include <cstdio>

int main() {
    std::string command = "python robot.py";

    std::array<char, 128> buffer;
    std::string result;
    FILE* pipe = popen(command.c_str(), "r");
    if (pipe) {
        while (!feof(pipe)) {
            if (fgets(buffer.data(), 128, pipe) != nullptr) {
                result += buffer.data();
            }
        }
        pclose(pipe);
    }

    std::cout << result << std::endl;
    return 0;
}

