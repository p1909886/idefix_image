import sys
import os

absolute_path = os.path.dirname(__file__)
relative_path = "modules"
full_path = os.path.join(absolute_path, relative_path)
sys.path.append(full_path)

import argparse
import cv2
import numpy as np
from typing import Tuple

from networking import TCPClientAbstraction, DisconnectedException
from encoding import Packer, PackTraits

class JpegImage(PackTraits):
    TYPE_NAME   =   'cv::Mat'
    CODE        =   'JP'
    LENGTH      =   -1
    QUALITY     =   90
    @classmethod
    def pack(cls,value: list)->str:
        encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), cls.QUALITY]
        result, buffer = cv2.imencode('.jpg', value, encode_param)
        temp = [
            Packer.pack(len(buffer)),
            buffer
        ]
        return b''.join(temp)
    @classmethod
    def unpack(cls,buffer:str,index:int)->Tuple[int,list]:
        try:
            index, lg = Packer.unpack(buffer,index)
            tmp = buffer[index:(index+lg)]
            mat = cv2.imdecode(np.asarray(tmp),cv2.IMREAD_COLOR)
            return index+lg, mat
        except Exception:
            raise

class Client(TCPClientAbstraction):
    def __init__(self):
        super().__init__(2048)
        self.size = None
        self.frame = None
    def incomingMessage(self,buffer):
        if buffer is None:
            self.stop()
            return
        if buffer.length == 0:
            self.stop()
            return
        index, self.frame = Packer.unpack(buffer.buffer,0)
    def start(self,args):
        self.initialize(args.server,args.port)
        buffer = self.receive()
        if buffer is None:
            self.finalize()
            return
        if buffer.length == 0:
            self.finalize()
            return
        print(buffer.displayString())
        index, size = Packer.unpack(buffer.buffer, 0)
        self.passiveReceive(self.incomingMessage)
    def stop(self):
        self.finalize()


if __name__ == "__main__":
    client = Client()
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--server', action='store', default='127.0.0.1', type=str, help='address of server to connect')
    parser.add_argument('-p', '--port', action='store', default=12345, type=int, help='port on server')
    args = parser.parse_args()

    try:
        client.start(args)
        while client.connected:
            if client.frame is not None:
                cv2.imshow('test',client.frame)
            key = cv2.waitKey(1) & 0x0FF
            if key == ord('x'): break
        client.stop()
    except DisconnectedException:
        print("Plantage du serveur et/ou de la connexion")
        client.stop()